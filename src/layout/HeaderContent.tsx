import { createElement, useState, useContext } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IHeader extends IFunctionComponent {}

const Header: React.FunctionComponent<IHeader> = ({ className }) => {
  return <div className={className}>Weather Forecast</div>;
};

const styledComp = styled(Header)`
  ${({ theme }) => theme.mixin("flex")()}
  ${({ theme }) => theme.mixin("boxPadding")("10px 30px")}
  ${({ theme }) => theme.mixin("font")(theme.global("white"), "22px")}
  width: 100%;
`;

export default styledComp;
