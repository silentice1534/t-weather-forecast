export const weatherTableConfig = [
  {
    label: "Date",
    prop: "date",
    width: 100,
  },
  {
    label: "Max Temp(°C)",
    prop: "maxTemp",
    width: 100,
  },
  {
    label: "Min Temp(°C)",
    prop: "minTemp",
    width: 100,
  },
  {
    label: "Humidity(%)",
    prop: "humidity",
    width: 100,
  },
];
