const Piechart = function (options) {
  const { canvasId, data, colors } = options;

  this.options = options;
  this.canvas = document.getElementById(canvasId);
  this.ctx = this.canvas.getContext("2d");
  this.colors = colors;
  this.data = data;

  function drawPieSlice(
    ctx,
    centerX,
    centerY,
    radius,
    startAngle,
    endAngle,
    color
  ) {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
  }

  this.draw = function () {
    var totalValue = 0;
    var colorIndex = 0;
    for (var categ in this.data) {
      var val = this.data[categ];
      totalValue += val;
    }

    let startAngle = 0;

    const labelX = this.canvas.width / 2;
    const labelY = this.canvas.height / 2;

    for (let date in this.data) {
      const val = this.data[date];
      const sliceAngle = (2 * Math.PI * val) / totalValue;

      drawPieSlice(
        this.ctx,
        this.canvas.width / 2,
        this.canvas.height / 2,
        Math.min(this.canvas.width / 2, this.canvas.height / 2),
        startAngle,
        startAngle + sliceAngle,
        this.colors[colorIndex % this.colors.length]
      );

      this.ctx.fillStyle = "#006064";
      this.ctx.font = "16px Calibri";

      this.ctx.textAlign = "center";

      startAngle += sliceAngle;
      colorIndex++;
    }

    this.ctx.textAlign = "center";
    this.ctx.fillText(`${100 - val}%`, labelX, labelY + 5);
  };
};

export default Piechart;
