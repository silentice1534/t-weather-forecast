function BarChart(config) {
  const {
    canvasId,
    data,
    dailyHighTempColor,
    dailyLowTempColor,
    minValue,
    maxValue,
    gridLineIncrement,
  } = config;

  // user defined properties
  this.canvas = document.getElementById(canvasId);
  this.data = data;
  this.dailyHighTempColor = dailyHighTempColor;
  this.dailyLowTempColor = dailyLowTempColor;
  this.gridLineIncrement = gridLineIncrement;
  this.maxValue = maxValue - Math.floor(maxValue % this.gridLineIncrement);
  this.minValue = minValue;

  // constants

  this.axisColor = "#555";
  this.gridColor = "black";
  this.padding = 5;

  // relationships
  this.context = this.canvas.getContext("2d");
  this.context.clearRect(0, 0, 100, 100);
  this.range = this.maxValue - this.minValue;
  this.numGridLines = this.numGridLines = Math.round(
    this.range / this.gridLineIncrement
  );

  this.longestValueWidth = this.getLongestValueWidth();
  this.x = this.padding + this.longestValueWidth;
  this.y = this.padding * 2;
  this.width = this.canvas.width - (this.longestValueWidth + this.padding * 2);
  this.barWidth = (this.width / data.length) * 0.4;
  // this.height =
  //   this.canvas.height - (this.getLabelAreaHeight() + this.padding * 4);
  this.height = this.canvas.height;

  this.drawBars();
}

BarChart.prototype.getLabelAreaHeight = function () {
  let maxLabelWidth = 0;

  for (let n = 0; n < this.data.length; n++) {
    const label = this.data[n].date;
    maxLabelWidth = Math.max(
      maxLabelWidth,
      this.context.measureText(label).width
    );
  }

  return Math.round(maxLabelWidth / Math.sqrt(2));
};

BarChart.prototype.getLongestValueWidth = function () {
  let longestValueWidth = 0;
  for (let n = 0; n <= this.numGridLines; n++) {
    const value = this.maxValue - n * this.gridLineIncrement;
    longestValueWidth = Math.max(
      longestValueWidth,
      this.context.measureText(value).width
    );
  }
  return longestValueWidth;
};

BarChart.prototype.drawBars = function () {
  const context = this.context;
  context.save();
  const data = this.data;
  const barSpacing = this.width / data.length;
  const unitHeight = this.height / this.range;

  for (let n = 0; n < data.length; n++) {
    // 高溫
    const maxTemp = (data[n].maxTemp - this.minValue) * unitHeight;
    context.save();
    context.translate(
      Math.round(this.x + (n + 1 / 7) * barSpacing),
      Math.round(this.y + this.height)
    );
    context.scale(1, -1);
    context.beginPath();
    context.rect(-this.barWidth / 2, 0, this.barWidth, maxTemp);
    context.fillStyle = this.dailyHighTempColor;
    context.fill();
    context.restore();

    context.font = "16px Calibri";
    context.fillStyle = "#EF5350";
    context.textAlign = "center";
    context.fillText(
      `${data[n].maxTemp}`,
      this.x + ((n + 1) / 7) * barSpacing,
      this.y + this.height - maxTemp - 5
    );

    // 低溫
    const minTemp = (data[n].minTemp - this.minValue) * unitHeight;
    context.save();
    context.translate(
      Math.round(this.x + (n + 6 / 7) * barSpacing),
      Math.round(this.y + this.height)
    );
    context.scale(1, -1);
    context.beginPath();
    context.rect((-this.barWidth / 3) * 2, 0, this.barWidth, minTemp);
    context.fillStyle = this.dailyLowTempColor;
    context.fill();

    context.restore();

    context.font = "24px";
    context.textAlign = "center";
    context.fillStyle = "#42A5F5";
    context.fillText(
      `${data[n].minTemp}`,
      this.x + ((n + 5.5) / 7) * barSpacing,
      this.y + this.height - minTemp - 5
    );
  }
  context.restore();
};

export default BarChart;
