import request from "@/api";

export const weatherAPI = async (payload) => {
  const res = await request({
    method: "post",
    url: "",
    data: payload,
  });

  return res.data;
};
