import restful from "@/api/libs/restful";

const baseURL = import.meta.env.VITE_API_BASE_URL;

const request = restful({
  baseURL,
  version: "",
  useAuth: false,
  timeout: 10000,
});

export default request;
