import { useEffect } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import BarChartFn from "@/utils/barChart";
import { IBarChartConfig, ICanvasAttribute } from "@/interface/IChartConfig";
import { v4 as uuid } from "uuid";

interface IBarChart extends IFunctionComponent {
  barChartConfig: IBarChartConfig;
  canvasAttribute: ICanvasAttribute;
}

const BarChart: React.FunctionComponent<IBarChart> = ({
  className,
  barChartConfig,
  canvasAttribute,
}) => {
  const id = uuid();

  useEffect(() => {
    new BarChartFn({ ...barChartConfig, canvasId: id });
  }, [barChartConfig, canvasAttribute]);

  return (
    <div className={className}>
      <canvas {...canvasAttribute} id={id}></canvas>
    </div>
  );
};

const styledComp = styled(BarChart)``;

export default styledComp;
