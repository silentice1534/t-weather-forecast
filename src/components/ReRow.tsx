import styled from 'styled-components';
import { IFunctionComponent } from '@/interface/IFunctionComponent';
import React from 'react';

interface IReRow extends IFunctionComponent {
  mt?: boolean | true;
  justifyContent?: string;
  alignItems?: string;
  flexDirection?: string;
}

const ReRow: React.FunctionComponent<IReRow> = ({ className, children, mt = true }) => {
  return <div className={`re-row ${className}`}>{children}</div>;
};

const StyledComp = styled(ReRow)<IReRow>`
  display: flex;
  align-items: ${({ alignItems = 'center' }) => alignItems};
  justify-content: ${({ justifyContent = 'flex-start' }) => justifyContent};
  flex-direction: ${({ flexDirection = 'row' }) => flexDirection};
  width: 100%;
  margin-top: ${({ mt = true }) => (mt ? '10px' : '0')};
  margin-bottom: 30px;
  text-align: right;
`;

export default StyledComp;
