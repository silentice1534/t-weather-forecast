import { useState, useRef } from "react";
import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { BsCaretDownFill } from "react-icons/bs";

import ReSelectOption from "@/components/ReSelectOption";
import useOutsideClick from "@/hooks/useOutSideClick";

interface IReSelect extends IFunctionComponent {
  options: any[];
  pager?: any;
  handlePageSize: (pageSize: any) => void;
}

const ReSelect: React.FunctionComponent<IReSelect> = ({
  className,
  options,
  handlePageSize,
  pager = {
    pageIndex: 1,
    totalPage: 1,
    pageSize: 10,
  },
}) => {
  const [isExpand, setIsExpand] = useState(false);
  const selectRef = useRef(null);

  const optionList = options.map((option: any) => {
    return (
      <ReSelectOption
        option={option}
        key={option.value}
        handlePageSize={handlePageSize}
      />
    );
  });

  useOutsideClick(selectRef, () => {
    setIsExpand(false);
  });

  return (
    <div className={`re-select ${className}`}>
      <div
        className="re-select-field"
        ref={selectRef}
        onClick={() => {
          setIsExpand((prev) => !prev);
        }}
      >
        <p className="re-select-field__text">{pager.pageSize}</p>
        <div className={`expand-down ${isExpand && `expand-down--active`}`}>
          <BsCaretDownFill className="expand-down__icon" />
        </div>
      </div>

      {isExpand && (
        <div className="re-select-option-content">
          <ul className="re-select-option-content__list">{optionList}</ul>
        </div>
      )}
    </div>
  );
};

const StyledComp = styled(ReSelect)<IReSelect>`
  position: relative;
  width: 50px;

  .re-select-field {
    position: relative;

    &__text {
      width: 100%;
      cursor: pointer;
      border: 0px transparent;
      line-height: 30px;
      outline: transparent;
      ${({ theme: { mixin } }) => mixin("boxPadding")("2px")}
      ${({ theme }) => theme.mixin("font")(theme.text.basic, "14px")}
      border-bottom: 1px solid ${({ theme: { global } }) => global("main")};
    }
  }

  .re-select-option-content {
    ${({ theme: { mixin } }) => mixin("position")("tl", "100%", "0")};
    z-index: 100;
    margin-top: 5px;
    background-color: ${({ theme: { global } }) => global("white")};
  }

  .expand-down {
    ${({ theme: { mixin } }) => mixin("position")("tr", "50%", "10px")};
    transform: translateY(-50%) rotate(180deg);
    transition: 0.4s;

    &__icon {
      font-size: 12px;
    }

    &--active {
      transform: translateY(-50%) rotate(360deg);
    }
  }
`;

export default StyledComp;
