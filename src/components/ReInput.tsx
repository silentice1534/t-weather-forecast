import { useState } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IReInput extends IFunctionComponent {
  value: string;
  onChange: (value: any) => void;
  placeholder: string;
}

const ReInput: React.FunctionComponent<IReInput> = ({
  className,
  value,
  onChange,
  placeholder = "",
}) => {
  // const [keyword, setKeyword] = useState<string>('');

  return (
    <div className={`re-input ${className}`}>
      <input
        className="re-input__native"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
      />
    </div>
  );
};

const StyledComp = styled(ReInput)`
  height: 32px;
  ${({ theme: { mixin } }) => mixin("flex")("flex-start", "flex-end")}

  .re-input {
    &__native {
      width: 100%;
      box-sizing: border-box;
      border: 0;
      border-bottom: 1px solid #565656;
      background-color: transparent;
      outline: 0;
      line-height: 31px;
    }
  }
`;

export default StyledComp;
