import { useState, useEffect } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import BarChart from "@/components/BarChart";
import PieChart from "@/components/PieChart";

import {
  IBarChartConfig,
  ICanvasAttribute,
  IPieChartConfig,
} from "@/interface/IChartConfig";

interface ITodayWeather extends IFunctionComponent {
  data: any;
  barChartConfig: IBarChartConfig;
  barChartCanvasConfig: ICanvasAttribute;
  pieChartConfig: IPieChartConfig;
  pieChartCanvasConfig: ICanvasAttribute;
}

const TodayWeather: React.FunctionComponent<ITodayWeather> = ({
  data,
  className,
  barChartConfig,
  barChartCanvasConfig,
  pieChartConfig,
  pieChartCanvasConfig,
}) => {
  return (
    <div className={`today-weather__item ${className}`}>
      <div className="today-weather__item__info">
        <p className="weather-date">今日({data.date})</p>

        <div className="weather-icon">
          <img
            className="weather-icon__img"
            src={`https://www.metaweather.com/static/img/weather/${data.weatherStateAbbr}.svg`}
          />
        </div>
      </div>
      <div className="today-weather__item__temp">
        <p className="title">Temp.</p>
        <BarChart
          barChartConfig={{ ...barChartConfig, data: [data] }}
          canvasAttribute={barChartCanvasConfig}
        />
      </div>
      <div className="today-weather__item__humidity">
        <p className="title">Humidity.</p>
        <PieChart
          pieChartConfig={{
            ...pieChartConfig,
            data: {
              [data.date]: data.humidity,
              empty: 100 - data.humidity,
            },
          }}
          canvasAttribute={pieChartCanvasConfig}
        />
      </div>
    </div>
  );
};

const styledComp = styled(TodayWeather)`
  ${({ theme: { mixin } }) => mixin("boxPadding")("15px 10px")}
  ${({ theme: { mixin } }) => mixin("flex")("flex-start", "flex-start")}

  

  .today-weather {
    &__item {
      &__info {
      }

      &__temp {
        margin-left: 30px;
      }

      &__humidity {
        margin-left: 30px;
      }
    }
  }

  .weather-icon {
    width: 80px;

    &__img {
      width: 100%;
    }
  }

  .title {
    ${({ theme: { mixin, global } }) => mixin("font")(global("grey"), "12px")}

    &--mb {
      margin-bottom: 5px;
    }
  }

  .weather-date {
    display: inline-block;
    ${({ theme: { mixin } }) => mixin("boxPadding")("5px")}
    ${({ theme: { mixin, global } }) => mixin("font")(global("main"), "18px")}
    position: relative;
    background-color: ${({ theme: { global } }) => global("grey")};
    border-radius: 8px;
    margin-bottom: 20px;
  }
`;

export default styledComp;
