import { useState } from "react";
import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IReCheckbox extends IFunctionComponent {
  mt?: boolean;
}

const ReCheckbox: React.FunctionComponent<IReCheckbox> = ({ className }) => {
  const [isCheck, setIsCheck] = useState(false);
  const handleIsCheck = () => {
    setIsCheck((preValue) => !preValue);
  };

  return (
    <div className={`re-checkbox-box ${className}`} onClick={handleIsCheck}>
      <div className="re-checkbox-box__input">
        <span
          className={`re-checkbox-box__input__actural ${
            isCheck && "re-checkbox-box__input__actural--active"
          }`}
        />
      </div>
    </div>
  );
};

const StyledComp = styled(ReCheckbox)<IReCheckbox>`
  ${({ theme }) => theme.mixin("font")(theme.text.basic, "24px")}

  .re-checkbox-box {
    display: inline-flex;
    cursor: pointer;

    &__input {
      ${({ theme }) => theme.mixin("circle")("16px", "2px")};
      ${({ theme: { mixin } }) => mixin("boxPadding")("2px")}
      display: inline-block;
      position: relative;
      vertical-align: middle;
      border: 1px solid ${({ theme: { global } }) => global("main")};

      &__native {
        display: none;
      }

      &__actural {
        ${({ theme }) => theme.mixin("position")("center")};
        ${({ theme }) => theme.mixin("circle")("10px", "2px")};
        display: inline-block;

        &--active {
          background-color: ${({ theme: { global } }) => global("assist")};
        }
      }
    }
  }
`;

export default StyledComp;
