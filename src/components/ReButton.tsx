import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import React from "react";

type ButtonHTMLType = "submit" | "button" | "reset";

interface IReButton extends IFunctionComponent {
  type?: ButtonHTMLType | "button";
  onClick?: (e: React.MouseEvent) => void;
}

const ReButton: React.FunctionComponent<IReButton> = ({
  className,
  children,
  type = "button",
  onClick = () => {},
}) => {
  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    const { type }: { type: string } = e.target as HTMLButtonElement;

    if (type === "button") {
      onClick(e);
    }
  };

  return (
    <div className={`re-button ${className}`}>
      <button className="re-button__native" type={type} onClick={handleClick}>
        {children}
      </button>
    </div>
  );
};

const StyledComp = styled(ReButton)`
  display: inline-block;
  text-align: right;

  & + & {
    margin-left: 10px;
  }

  .re-button {
    &__native {
      ${({ theme: { mixin } }) => mixin("flex")("center")}
      ${({ theme: { mixin, global } }) =>
        mixin("font")(global("mainLight"), "14px")}
      height: 32px;
      border: 1px solid ${({ theme: { global } }) => global("mainLight")};
      background-color: ${({ theme: { global } }) => global("white")};
      transition: 0.4s;
      cursor: pointer;
    }
  }
`;

export default StyledComp;
