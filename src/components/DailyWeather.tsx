import { useState, useEffect } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import BarChart from "@/components/BarChart";
import PieChart from "@/components/PieChart";

import {
  IBarChartConfig,
  ICanvasAttribute,
  IPieChartConfig,
} from "@/interface/IChartConfig";

interface IDailyWeather extends IFunctionComponent {
  data: any;
  barChartConfig: IBarChartConfig;
  barChartCanvasConfig: ICanvasAttribute;
  pieChartConfig: IPieChartConfig;
  pieChartCanvasConfig: ICanvasAttribute;
}

const DailyWeather: React.FunctionComponent<IDailyWeather> = ({
  data,
  className,
  barChartConfig,
  barChartCanvasConfig,
  pieChartConfig,
  pieChartCanvasConfig,
}) => {
  return (
    <li className={`daily-weather__item ${className}`}>
      <div className="daily-weather__item__info">
        <span className="weather-date">{data.date}</span>

        <div className="weather-icon">
          <img
            className="weather-icon__img"
            src={`https://www.metaweather.com/static/img/weather/${data.weatherStateAbbr}.svg`}
          />
        </div>
      </div>
      <div className="daily-weather__item__temp">
        <p className="title">Temp.</p>
        <BarChart
          barChartConfig={{ ...barChartConfig, data: [data] }}
          canvasAttribute={barChartCanvasConfig}
        />
      </div>
      <div className="daily-weather__item__humidity">
        <p className="title  title--mb">Humidity.</p>
        <PieChart
          pieChartConfig={{
            ...pieChartConfig,
            data: {
              [data.date]: data.humidity,
              empty: 100 - data.humidity,
            },
          }}
          canvasAttribute={pieChartCanvasConfig}
        />
      </div>
    </li>
  );
};

const styledComp = styled(DailyWeather)`
  ${({ theme: { mixin } }) => mixin("boxPadding")("15px 30px")}

  & + & {
    border-left: 1px solid #efefef;
  }

  .daily-weather {
    &__item {
      &__info {
        height: 70px;
        ${({ theme: { mixin, global } }) =>
          mixin("font")(global("main"), "16px")}
        position: relative;
      }

      &__temp {
        margin: 20px 0;
      }

      &__humidity {
      }
    }
  }

  .weather-icon {
    ${({ theme: { mixin } }) => mixin("position")("br", "0px", "0px")};
    width: 50px;

    &__img {
      width: 100%;
    }
  }

  .title {
    ${({ theme: { mixin, global } }) => mixin("font")(global("grey"), "12px")}

    &--mb {
      margin-bottom: 10px;
    }
  }
`;

export default styledComp;
