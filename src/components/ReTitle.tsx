import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IReTitle extends IFunctionComponent {
  mt?: boolean;
}

const ReTitle: React.FunctionComponent<IReTitle> = ({
  className,
  children,
}) => {
  return <p className={className}>{children}</p>;
};

const StyledComp = styled(ReTitle)<IReTitle>`
  ${({ theme }) => theme.mixin("font")(theme.text.basic, "24px")}
`;

export default StyledComp;
