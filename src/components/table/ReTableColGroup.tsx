import styled from 'styled-components';
import { IFunctionComponent } from '@/interface/IFunctionComponent';

interface IReTableColGroup extends IFunctionComponent {
  // tableData: any[];
  columnConfig: any[];
}

const ReTableColGroup: React.FunctionComponent<IReTableColGroup> = ({ className, children, columnConfig }) => {
  const rendCols = columnConfig.map((col) => {
    return <col className="re-table-col-group__content" key={col.prop} width={col.width}></col>;
  });

  return <colgroup className="re-table-col-group">{rendCols}</colgroup>;
};

const StyledComp = styled(ReTableColGroup)`
  .re-table-col-group {
    &__content {
      box-sizing: border-box;
    }
  }
`;

export default StyledComp;
