import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import ReTableColGroup from "@/components/table/ReTableColGroup";
import ReTableHeader from "@/components/table/ReTableHeader";
import ReTableBody from "@/components/table/ReTableBody";

interface IReTable extends IFunctionComponent {
  tableData: any[];
  columnConfig: any[];
}

const ReTable: React.FunctionComponent<IReTable> = ({
  className,
  children,
  columnConfig,
  tableData,
}) => {
  return (
    <div className={`re-table ${className}`}>
      <div className="re-table__content">
        <table className="table">
          <ReTableColGroup columnConfig={columnConfig} />
          <ReTableHeader columnConfig={columnConfig} />
          <ReTableBody tableData={tableData} columnConfig={columnConfig} />
        </table>
      </div>
    </div>
  );
};

const StyledComp = styled(ReTable)<IReTable>`
  margin-top: 30px;

  .re-table {
    &__content {
      box-sizing: border-box;
      overflow-x: auto;
    }
  }

  .table {
    width: 100%;
    table-layout: fixed;
  }
`;

export default StyledComp;
