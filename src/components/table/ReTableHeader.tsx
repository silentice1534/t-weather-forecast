import ReCheckbox from "@/components/ReCheckbox";
import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { convertToObject } from "typescript";

interface IReTableHeader extends IFunctionComponent {
  // tableData: any[];
  columnConfig: any[];
}

const ReTableHeader: React.FunctionComponent<IReTableHeader> = ({
  className,
  children,
  columnConfig,
}) => {
  const renderTds = columnConfig.map((col) => {
    return (
      <td
        className="re-table-header__tr__td"
        key={col.prop}
        width={`${col.width || 150}px`}
      >
        <div className="re-table-header__tr__td__content">
          {col.renderType === "checkbox" ? <ReCheckbox /> : col.label}
        </div>
      </td>
    );
  });

  return (
    <thead className={`re-table-header ${className}`}>
      <tr className="re-table-header__tr">{renderTds}</tr>
    </thead>
  );
};

const StyledComp = styled(ReTableHeader)`
  .re-table-header {
    &__tr {
      border-bottom: 2px solid ${({ theme }) => theme.table.trDivide};

      &__td {
        &__content {
          ${({ theme }) => theme.mixin("font")(theme.text.basic, "14px")}
          ${({ theme: { mixin, global } }) => mixin("boxPadding")("20px 10px")}
          display: inline-block;
        }
      }
    }
  }
`;

export default StyledComp;
