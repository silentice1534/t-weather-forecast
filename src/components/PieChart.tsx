import { useEffect } from "react";
import styled from "styled-components";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import PieChartFn from "@/utils/pieChart";
import { IPieChartConfig, ICanvasAttribute } from "@/interface/IChartConfig";
import { v4 as uuid } from "uuid";

interface IPieChart extends IFunctionComponent {
  pieChartConfig: IPieChartConfig;
  canvasAttribute: ICanvasAttribute;
}

const PieChart: React.FunctionComponent<IPieChart> = ({
  className,
  pieChartConfig,
  canvasAttribute,
}) => {
  const id = uuid();

  useEffect(() => {
    var humidityPiechart = new PieChartFn({ ...pieChartConfig, canvasId: id });
    humidityPiechart.draw();
  }, [pieChartConfig, canvasAttribute]);

  return (
    <div className={className}>
      <canvas {...canvasAttribute} id={id}></canvas>
    </div>
  );
};

const styledComp = styled(PieChart)``;

export default styledComp;
