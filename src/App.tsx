import { useState } from "react";

// pages

// component
import Lo from "@/layout/layout";
import HeaderContent from "@/layout/HeaderContent";

// router
import { BrowserRouter, Switch } from "react-router-dom";
import routes from "./router";

// style
import "@/styles/scss/index.scss";
// import "@/styles/index.scss";

// theme
import { ThemeProvider } from "styled-components";
import { usedThemeSetting } from "@/styles/theme";

// pages

function App() {
  const [theme, setTheme] = useState(usedThemeSetting);

  return (
    <div className="app">
      <BrowserRouter>
        <Switch>
          <ThemeProvider theme={theme}>
            <Lo.Layout>
              <Lo.Header>
                <HeaderContent />
              </Lo.Header>
              <Lo.Views>{routes}</Lo.Views>
            </Lo.Layout>
          </ThemeProvider>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
