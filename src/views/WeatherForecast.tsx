import { useState, useEffect, useMemo } from "react";
import styled from "styled-components";

import { AiOutlineLoading3Quarters } from "react-icons/ai";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { weatherAPI } from "@/api/weather";
import { weatherTableConfig } from "@/config/weatherTable";

import ReRow from "@/components/ReRow";
import ReTable from "@/components/table";
import ReInput from "@/components/ReInput";
import ReButton from "@/components/ReButton";
import BarChart from "@/components/BarChart";
import PieChart from "@/components/PieChart";
import DailyWeather from "@/components/DailyWeather";
import TodayWeather from "@/components/TodayWeather";
// import ReModal from "@/components/ReModal";
import {
  IBarChartConfig,
  ICanvasAttribute,
  IPieChartConfig,
} from "@/interface/IChartConfig";

const baseURL = import.meta.env.VITE_WHEATHER_FORECAST_BASE_URL;

type TLocationData = {
  title: string;
  location_type: string;
  latt_long: string;
  woeid: string;
  distance: string;
};

type visisbleType = "visible" | "invisible" | "init" | "loading";

interface IWeatherData {
  date: string;
  id: number;
  maxTemp: number;
  minTemp: number;
  humidity: number;
  weatherStateAbbr: string;
}

interface IApiWeatherData extends IWeatherData {
  [key: string]: any;
}

interface IWeatherForeCast extends IFunctionComponent {}

const WeatherForecast: React.FunctionComponent<IWeatherForeCast> = ({
  className,
}) => {
  const [todayData, setTodayData] = useState<IWeatherData>({
    date: "",
    id: 0,
    maxTemp: 0,
    minTemp: 0,
    humidity: 0,
    weatherStateAbbr: "",
  });
  const [tableData, setTableData] = useState<IWeatherData[]>([]);
  const [locationData, setLocationData] = useState<TLocationData>({
    title: "",
    location_type: "",
    latt_long: "",
    woeid: "",
    distance: "",
  });
  const [keyword, setKeyword] = useState<string>("");
  const [visibleType, setVisibleType] = useState<visisbleType>("init");

  const [barChartConfig, setBarChartConfig] = useState<IBarChartConfig>({
    canvasId: "barChartCanvas",
    data: tableData,
    dailyHighTempColor: "#EF5350",
    dailyLowTempColor: "#42A5F5",
    minValue: 0,
    maxValue: 100,
    gridLineIncrement: 5,
  });
  const [barChartCanvasConfig, setBarChartCanvasConfig] =
    useState<ICanvasAttribute>({
      id: "barChartCanvas",
      width: "100",
      height: "100",
    });

  const [pieChartConfig, setPieChartConfig] = useState<IPieChartConfig>({
    canvasId: "pieChartCanvas",
    data: {},
    colors: ["#26C6DA", "#fff"],
  });

  const [pieChartCanvasConfig, setPieChartCanvasConfig] =
    useState<ICanvasAttribute>({
      id: "pieChartCanvas",
      width: "80",
      height: "80",
    });

  const [contentWidth, setContentWidth] = useState(window.innerWidth - 60);

  const handlekeyword = (e: any) => {
    setKeyword(e.target.value);
  };

  const handleClick = () => {
    getLocationData();
  };

  const getLocationData = async () => {
    setVisibleType("loading");

    if (keyword === "") {
      setVisibleType("invisible");
      return;
    }

    try {
      const res: any = await weatherAPI({
        method: "get",
        url: `${baseURL}/location/search/?query=${keyword}`,
      });

      if (res?.data?.length) {
        const [locationInfo] = res.data;

        setLocationData(locationInfo);
        return;
      }

      setVisibleType("invisible");
    } catch (e) {
      console.log("error", e);
      setVisibleType("invisible");
    }
  };

  const getWeatherForecast = async () => {
    const { woeid }: TLocationData = locationData;

    try {
      const res: any = await weatherAPI({
        method: "get",
        url: `${baseURL}/location/${woeid}`,
      });

      const { consolidated_weather: consolidatedWeather } = res.data;
      const weatherData: IWeatherData[] = consolidatedWeather.map(
        (item: IApiWeatherData) => {
          let {
            applicable_date,
            id,
            max_temp: maxTemp,
            min_temp: minTemp,
            humidity,
            weather_state_abbr: weatherStateAbbr,
          } = item;

          maxTemp = maxTemp.toFixed(2);
          minTemp = minTemp.toFixed(2);

          const [y, m, d]: string[] = applicable_date.split("-");
          const date = `${m}/${d}`;

          return {
            date,
            id,
            maxTemp,
            minTemp,
            humidity,
            weatherStateAbbr,
          };
        }
      );

      const { maxTemp, minTemp }: { maxTemp: number; minTemp: number } =
        weatherData.reduce(
          (obj, item) => {
            if (item.maxTemp > obj.maxTemp) {
              obj.maxTemp = Math.ceil(item.maxTemp);
            }

            if (item.minTemp < obj.minTemp) {
              obj.minTemp = Math.ceil(item.minTemp);
            }

            return obj;
          },
          { minTemp: 0, maxTemp: 0 }
        );

      const pieChartData = weatherData.reduce(
        (obj: { [key: string]: number }, item) => {
          obj[item.date] = item.humidity;

          return obj;
        },
        {}
      );

      setTodayData(
        weatherData.shift() || {
          date: "",
          id: 0,
          maxTemp: 0,
          minTemp: 0,
          humidity: 0,
          weatherStateAbbr: "",
        }
      );
      setTableData(weatherData);

      setBarChartConfig((prev) => {
        return {
          ...prev,
          data: weatherData,
          maxValue: maxTemp + 5,
          minValue: minTemp >= 0 ? minTemp : minTemp - 5,
        };
      });

      setPieChartConfig((prev) => {
        return {
          ...prev,
          data: pieChartData,
        };
      });

      setVisibleType("visible");
    } catch (e) {
      console.log("error", e);
      setVisibleType("invisible");
    }
  };

  useEffect(() => {
    function handleResize() {
      setContentWidth(window.innerWidth - 60);
    }

    window.addEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (locationData.woeid) {
      getWeatherForecast();
    }
  }, [locationData]);

  useEffect(() => {
    let width: string = "";
    if (contentWidth <= 0) {
      width = String(contentWidth);
    } else {
      width = contentWidth > 0 ? "100" : String((contentWidth - 10) / 2);
    }

    setPieChartCanvasConfig((prev) => ({
      ...prev,
      width,
    }));

    setBarChartCanvasConfig((prev) => ({
      ...prev,
      width,
    }));
  }, [contentWidth]);

  const dailyWeatherRender = () => {
    return tableData.map((data) => {
      return (
        <DailyWeather
          data={data}
          barChartConfig={barChartConfig}
          barChartCanvasConfig={barChartCanvasConfig}
          pieChartConfig={pieChartConfig}
          pieChartCanvasConfig={pieChartCanvasConfig}
          key={data.id}
        />
      );
    });
  };

  return (
    <div className={`weather-forecast ${className}`}>
      <ReRow alignItems="flex-end">
        <ReInput
          value={keyword}
          onChange={handlekeyword}
          placeholder="Please enter the city"
        />
        <div className="btn-wrap">
          <ReButton onClick={handleClick}>Search</ReButton>
        </div>
      </ReRow>

      {visibleType === "init" && <p></p>}
      {visibleType === "loading" && (
        <AiOutlineLoading3Quarters className="loading-icon" />
      )}
      {visibleType === "visible" && (
        <>
          <div className="today-weather">
            <TodayWeather
              data={todayData}
              barChartConfig={barChartConfig}
              barChartCanvasConfig={{
                id: "barChartCanvas",
                width: "150",
                height: "150",
              }}
              pieChartConfig={pieChartConfig}
              pieChartCanvasConfig={{
                id: "pieChartCanvas",
                width: "100",
                height: "150",
              }}
            />
          </div>

          <div className="weather-wrap">
            <ul className="daily-weather">{dailyWeatherRender()}</ul>
          </div>
        </>
      )}
      {visibleType === "invisible" && (
        <p className="error-message">Sorry, No Data Existed</p>
      )}
    </div>
  );
};

const StyledComp = styled(WeatherForecast)<IWeatherForeCast>`
  .btn-wrap {
    margin-left: 20px;
  }

  .loading-icon {
    animation: spin 2s infinite linear;
    color: ${({ theme: { global } }) => global("mainLight")};
    font-size: 32px;
  }

  .today-weather {
    margin-left: 30px;
  }

  .daily-weather {
    ${({ theme: { mixin } }) => mixin("flex")()}
    width: 100%;
    margin-top: 50px;
  }

  .error-message {
    ${({ theme: { mixin, global } }) => mixin("font")(global("error"), "16px")}
  }

  .chart-wrap {
    display: flex;
    flex-direction: column;

    .bar-chart {
      flex: 1;
    }

    .pie-chart {
      flex: 1;
    }
  }

  @media screen and (min-width: 900px) {
    .chart-wrap {
      display: flex;
      flex-direction: row;
    }
  }

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }

    to {
      transform: rotate(360deg);
    }
  }
`;

export default StyledComp;
