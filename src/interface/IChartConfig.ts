export interface ICanvasAttribute {
  id: string;
  width: string;
  height: string;
}

export interface IBarChartConfig {
  canvasId: string;
  data: { [key: string]: any };
  dailyHighTempColor: string;
  dailyLowTempColor: string;
  minValue: number;
  maxValue: number;
  gridLineIncrement: number;
}

export interface IPieChartConfig {
  canvasId: string;
  data: { [key: string]: number };
  colors: string[];
}
