export interface ISelectOption {
  value: string;
  label: string;
  i18nKeyLabel: string;
}
