export interface IFunctionComponent {
  className?: string;
  children?: React.ReactNode;
}
