import { Iroutes } from "@/interface/routes";

export const basic: Iroutes<string>[] = [
  {
    path: "/",
    component: "WeatherForecast",
    name: "WeatherForecast",
    exact: true,
  },
];

export const menu: Iroutes<string>[] = [];
